<?php 

/**
	* Plugin Name: UVjagtPro - Improved search field
	* Description: This plugin provides design changes to the search field, that is provided by the Storefront theme.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	include "custom-search-form.php";

	// ----------------------------------
	// Remove default search box from header	

	add_action( 'init', 'remove_sf_actions' );

	function remove_sf_actions() {

		remove_action( 'storefront_header', 'storefront_product_search', 40 );
	}

	// ----------------------------------
	// Display custom search field below header
	 
	add_action('storefront_before_content', 'add_search_below_header', 0);
	 
	function add_search_below_header() {

		custom_search_form(); 
	
	}

?>