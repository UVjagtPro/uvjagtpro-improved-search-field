<?php 

 
	function custom_search_form() {
		?>
		<div class="custom-search-field">

			<form role="search" method="get" id="customSearchform" action="<?php echo home_url( '/' ); ?>">
				
				<input class="custom-search-input" type="text" placeholder="Søg efter produkt..." name="s" id="s" />

				<input type="hidden" value="product" name="post_type" />

				<input type="submit" id="searchsubmit" value="Søg" />

			</form>
		</div>

		<?php 
	}

?>